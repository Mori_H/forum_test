package forum.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import forum.beans.User;
import forum.service.UserService;

@WebServlet("/signup")
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));

            new UserService().register(user);

            response.sendRedirect("userSettings");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");

        if (StringUtils.isEmpty(login_id) == true) {
            messages.add("ログインＩＤを入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
