<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録画面</title>
</head>
<body>
	<h1>ユーザー新規登録画面</h1>

	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<div>
				<label for="login_id">ログインＩＤ:</label> <input name="login_id"
					id="login_id" />
			</div>
			<div>
				<label for="password">登録用パスワード:</label> <input name="password"
					type="password" id="password" size="20" maxlength="20" class="text long"
					required />(半角英数字で6文字以上20文字以内)
			</div>
			<div>
				<label for="passwordConfirm">確認用パスワード:</label> <input
					name="passwordConfirm" type="password" id="passwordConfirm" size="20" maxlength="20"
					class="text long" required />

			</div>



			<br /> <input type="submit" value="登録" /> <br /> <a
				href="userSettings">戻る</a>
		</form>
	</div>
</body>
</html>